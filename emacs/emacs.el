;  (setq debug-on-error t)
  (setq user-full-name "Daniel Molina"
  user-mail-address "dmolina@decsai.ugr.es")

(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
;; Comment/uncomment this line to enable MELPA Stable if desired.  See `package-archive-priorities`
;; and `package-pinned-packages`. Most users will not need or want to do this.
;;(add-to-list 'package-archives ("melpa-stable" . "https://stable.melpa.org/packages/") t)
(package-initialize)

(setq use-package-always-ensure t)

(use-package vc-use-package
     :ensure t)

(setq delete-by-moving-to-trash t)
; (setq trash-directory "~/.Trash")

(use-package trashed
 :ensure t
 :bind ("C-c T" . trashed))

(setq inhibit-startup-message t)  ; Dont show the splash screen

(tool-bar-mode -1)
(scroll-bar-mode -1)

(make-directory "~/.emacs_backups/" t)
(make-directory "~/.emacs_autosave/" t)
(setq auto-save-file-name-transforms '((".*" "~/.emacs_autosave/" t)))
(setq backup-directory-alist '(("." . "~/.emacs_backups/")))

(setq backup-by-copying t)

(global-visual-line-mode)

;; Display line numbers in every buffer
(add-hook 'prog-mode-hook 'display-line-numbers-mode)

(global-hl-line-mode)

(fset 'yes-or-no-p 'y-or-n-p)

(setq display-time-24hr-format t)
  (display-time-mode 1)

(global-set-key (kbd "C-x C-b") 'ibuffer)

(use-package bookmark-frecency
:ensure t
:config
(bookmark-frecency-mode 1)
(setq bookmark-sort-flag 'last-modified)
)

(windmove-default-keybindings)

(use-package ace-window
    :ensure t
    :bind (("<f3>" . ace-window))
)

(setq gc-cons-threshold 100000000)

(setq dired-dwim-target t)

(add-hook 'dired-mode-hook 'dired-omit-mode)

(setq eshell-ls-use-colors t)

(setq eshell-list-files-after-cd t)

(setq eshell-ls-initial-args '("-hlt"))

(defadvice shell-command (after shell-in-new-buffer (command &optional output-buffer error-buffer))
    (when (get-buffer "*Async Shell Command*")
      (with-current-buffer "*Async Shell Command*"
         (rename-uniquely))))
 (ad-activate 'shell-command)

(unless
(and (boundp 'server-process)
   (processp server-process)
   (server-running-p))
(server-start))

(use-package proced
:custom (proced-auto-update-flag t))

(use-package which-key
  :ensure t
  :config
  (which-key-mode)
)

(use-package undo-tree
:diminish
:bind (("C-c _" . undo-tree-visualize))
:config
(global-undo-tree-mode +1)
(unbind-key "M-_" undo-tree-map))

(use-package recentf
:config
(add-to-list 'recentf-exclude "\\elpa")
(setq recentf-max-menu-items 50)
(setq recentf-max-saved-items 500)
(global-set-key "\C-x\ \C-r" 'recentf-open-files)
(recentf-mode 1))

(use-package recentf-ext
:ensure t)

(setq startup-redirect-eln-cache "~/Descargas/emacs/emacs.d")

(use-package smartparens
   :ensure t
:hook (prog-mode text-mode markdown-mode latex-mode) ;; add `smartparens-mode` to these hooks
:config
;; load default config
(require 'smartparens-config)
)

(use-package vterm
  :ensure t)

(use-package expand-region
    :ensure t
    :bind (("C-+" . er/expand-region)
           ("C--" . er/contract-region))
)

(use-package amx
  :ensure t
  :config
  (amx-mode)
  )

(use-package projectile
:ensure t
:init
(projectile-mode +1)
:bind (:map projectile-mode-map
	    ("s-p" . projectile-command-map)
	    ("C-c p" . projectile-command-map)))

(use-package consult-projectile
    :ensure t
    :after (projectile consult)
)

(use-package yasnippet
  :ensure t
  :config
  (yas-reload-all)
  :hook (prog-mode . yas-minor-mode)
  :hook (latex-mode . yas-minor-mode)
  :hook (org-mode . yas-minor-mode)
)

(use-package yasnippet-snippets
    :after yasnippet
    :ensure t
    :config
    (yas-reload-all)
)

(use-package nov
:ensure t
:mode ("\\.epub\\'" . nov-mode))

(use-package pdf-tools
      :ensure t
      :magic ("%PDF" . pdf-view-mode)
      :config
      (pdf-tools-install :no-query)
;; Recargo siempre
(add-hook 'pdf-view-mode-hook 'pdf-view-restore-mode)
)

(use-package pdf-view-restore
  :ensure t
  :after pdf-tools
  :config
  (add-hook 'pdf-view-mode-hook 'pdf-view-restore-mode))

(setq revert-without-query '(".pdf"))

(use-package org-pdftools
  :hook (org-mode . org-pdftools-setup-link))

(use-package markdown-mode
:ensure t
:mode ("\\.md\\'" . markdown-mode))

(setq dired-guess-shell-alist-user
    '(("\\.mp4" "mpv" "vlc")
      ("\\.wmv" "mpv" "vlc")))

(use-package dired-narrow
   :ensure t)

(use-package dired-recent
  :ensure t
  :init
  (dired-recent-mode 1)
)

(setq dired-clean-confirm-killing-deleted-buffers nil)
  (setq dired-no-confirm t)
  (setq dired-recursive-copies 'always)

(setq dired-listing-switches "-aBhl  --group-directories-first")

(use-package quick-find-files
  :defer t
  :ensure t
:vc (:fetcher github :repo "Phundrak/quick-find-files.el")
  :custom ; Depending on your preferences, of course
  (quick-find-files-program 'fd)
    (quick-find-files-dirs '((:dir "~/Descargas/emacs/" :ext "org")
			     (:dir "~/Descargas")
                             (:dir "~/working")))
  )

(use-package dired-quick-sort
  :ensure t
  :bind (:map dired-mode-map
	("C-c s" . 'hydra-dired-quick-sort/body))	
)

(use-package flyspell
  :defer t
  :init
    (flyspell-mode 1)
  :config
    (setq ispell-program-name "aspell")
    (setq ispell-list-command "--list") ;; run flyspell with aspell, not ispell
  :hook ((text-mode . flyspell-mode)
	 (prog-mode . flyspell-prog-mode))
 )

(use-package flyspell-correct
  :ensure t
  :after flyspell
  :bind (:map flyspell-mode-map ("C-;" . flyspell-correct-wrapper)))

(use-package flyspell-correct-popup
  :ensure t
  :after flyspell-correct
  :defer t)

(defun restart-flyspell-mode ()
  (when flyspell-mode
    (flyspell-mode-off)
    (flyspell-mode-on)))
(add-hook 'ispell-change-dictionary-hook 'restart-flyspell-mode)

(defun turn-on-flyspell () (flyspell-mode 1)) 
 (add-hook 'org-mode-hook 'turn-on-flyspell)
 (add-hook 'text-mode-hook 'turn-on-flyspell)

(use-package consult-flyspell
  :ensure t
  :config
  ;; default settings
  (setq consult-flyspell-select-function 'flyspell-correct-at-point
	consult-flyspell-set-point-after-word t
	consult-flyspell-always-check-buffer nil))

(use-package guess-language
  :ensure t
  :defer t
  :init (add-hook 'text-mode-hook #'guess-language-mode)
  :config
;; Optionally:
(setq guess-language-languages '(en es fr))
(setq guess-language-min-paragraph-length 35)
)

(use-package flycheck
    :ensure t
    :init (global-flycheck-mode)
)

(use-package langtool
  :ensure t
  :config
  (setq langtool-java-classpath
      "/usr/share/languagetool:/usr/share/java/languagetool/*")
)

(use-package flycheck-languagetool
  :ensure t
  :after langtool flycheck
  :hook (text-mode . flycheck-languagetool-setup)
  :init
  (setq flycheck-languagetool-server-jar "/usr/share/java/languagetool/languagetool-server.jar")
)

(use-package google-translate
  :ensure t
  :init
  (require 'google-translate-smooth-ui)
  :bind ("C-c G" . google-translate-smooth-translate))

(use-package nerd-icons
  ;; :custom
  ;; The Nerd Font you want to use in GUI
  ;; "Symbols Nerd Font Mono" is the default and is recommended
  ;; but you can use any other Nerd Font if you want
  ;; (nerd-icons-font-family "Symbols Nerd Font Mono")
  )

;; Descomentar para instalar
;; (nerd-icons-install-fonts)

(use-package doom-modeline
:ensure t
:after nerd-icons
:init (doom-modeline-mode 1))

(use-package modus-themes
  :ensure t
  :config
  ;; Load the Modus Light theme
  (load-theme 'modus-operandi t)
  ;; Allow to use variable-pitch-mode without problems in org-table and code blocks.
  (setq modus-themes-mixed-fonts t)
  )

(use-package spacious-padding
:ensure t
:config
(spacious-padding-mode 1))

(use-package dashboard
    :ensure t
    :after all-the-icons
    :config
    (dashboard-setup-startup-hook)
      ;; Content is not centered by default. To center, set
  (setq dashboard-center-content t)
  ;; use `all-the-icons` package
    (setq dashboard-icon-type 'all-the-icons)
  ;; To add icons to the widget headings and their items:
  ; (setq dashboard-set-heading-icons t)
; (setq dashboard-set-file-icons t)
    (setq dashboard-projects-backend 'projectile)
   (setq dashboard-items '(
		      (projects . 5)
		      (recents  . 5)
			(agenda . 5)
		      ; (bookmarks . 5)
		      (registers . 5)))
)

(use-package mixed-pitch
:hook
;; If you want it in all text modes:
(text-mode . mixed-pitch-mode))

(use-package all-the-icons
  :if (display-graphic-p)
  :hook (dired-mode . all-the-icons-dired-mode))

(use-package all-the-icons-ibuffer
  :after all-the-icons
  :ensure t
  :hook (ibuffer-mode . all-the-icons-ibuffer-mode))

(use-package ibuffer-projectile
  :ensure t
  :config
  (add-hook 'ibuffer-hook
    (lambda ()
      (ibuffer-projectile-set-filter-groups)
      (unless (eq ibuffer-sorting-mode 'alphabetic)
        (ibuffer-do-sort-by-alphabetic))))
)

(use-package org-modern
    :ensure t
    :hook ((org-mode . org-modern-mode))
)

(setq org-image-actual-width nil)

(setq org-ellipsis "  ▼")

(use-package vertico-posframe
:after vertico
:config
(vertico-posframe-mode 1)
)

(use-package org-fragtog
  :after org
  :hook
  (add-hook 'org-mode-hook 'org-fragtog-mode)
  :custom
  (org-format-latex-options
   (plist-put org-format-latex-options :scale 2)))

(use-package org-present
  :bind ("<f6>" . org-present)
  :bind (:map org-present-mode-keymap
	      ("C-<right>" . org-present-next)
	      ("C-<left>" . org-present-prev)))

(use-package org-bullets
  :no-require t
  :custom
  (org-bullets-bullet-list '("◉" "●" "○" "●" "○" "●")))

(use-package hide-lines)

(use-package hide-mode-line
  :defer t)

(defun terror/slide-setup ()
  (global-hl-line-mode -1)
  (setq org-hide-emphasis-markers t)
  (org-bullets-mode 1)
  (setq text-scale-mode-amount 3)
  (text-scale-mode 1)
  (set-frame-parameter (selected-frame)
		       'internal-border-width 75)
  (org-display-inline-images)
  (toggle-frame-fullscreen)
  (hide-mode-line-mode 1)
  (hide-lines-matching "#\\+begin")
  (hide-lines-matching "#\\+end"))

(defun terror/slide-end ()
  (global-hl-line-mode 1)
  (setq org-hide-emphasis-markers nil)
  (org-bullets-mode -1)
  (setq text-scale-mode-amount 0)
  (text-scale-mode -1)
  (set-frame-parameter (selected-frame)
		       'internal-border-width 20)
  (toggle-frame-fullscreen)
  (hide-mode-line-mode -1)
  (hide-lines-show-all)
  (org-fold-show-all))

(use-package org-tree-slide
  :after org
  :bind ("<f5>" . org-tree-slide-mode)
:bind (:map org-tree-slide-mode-map
	    ("C-<right>" . org-tree-slide-move-next-tree)
	    ("C-<left>" . org-tree-slide-move-previous-tree))
  :hook ((org-tree-slide-play . terror/slide-setup)
	 (org-tree-slide-stop . terror/slide-end))
  :config
  (setq org-tree-slide-slide-in-effect nil
	org-image-actual-width nil
	org-tree-slide-header t
	org-tree-slide-breadcrumbs " > "
	org-tree-slide-activate-message "Let's begin..."
	org-tree-slide-deactivate-message "The end :)"))

(use-package focus
  :ensure t
  :defer t)

(use-package writeroom-mode
    :ensure t
    :after focus
    :hook (writeroom-mode . focus-mode)
)

(use-package vc-fossil
  :ensure t
  ;; Keep from loading unnecessarily at startup.
  :defer t
  ;; This allows VC to load vc-fossil when needed.
  :init (add-to-list 'vc-handled-backends 'Fossil t))

(use-package magit
 :ensure t
 :bind ("C-x g" . magit-status))

(use-package git-gutter
 :ensure t
  :hook (prog-mode org-mode markdown-mode text-mode latex-mode)
  :custom (git-gutter:update-interval 0.02))

(use-package git-gutter-fringe
  :after git-gutter
  :config
  ;; https://ianyepan.github.io/posts/emacs-git-gutter/
    (define-fringe-bitmap 'git-gutter-fr:added [224] nil nil '(center repeated))
    (define-fringe-bitmap 'git-gutter-fr:modified [224] nil nil '(center repeated))
    (define-fringe-bitmap 'git-gutter-fr:deleted [128 192 224 240] nil nil 'bottom))

(use-package eglot-jl
:ensure t)

(org-babel-do-load-languages
 'org-babel-load-languages
 '((emacs-lisp . t)
   (R . t)
   (julia . t)))

(use-package web-mode
  :ensure t
  :mode (("\\.php$" .  web-mode)
	 ("\\.html$" .  web-mode))
)

(use-package rust-mode
  :ensure t
  :config
  (setq rust-format-on-save t)
(add-hook 'rust-mode-hook
	  (lambda () (prettify-symbols-mode)))
)

(use-package treesit-auto
      :ensure t
;;      :config
;;      (global-treesit-auto-mode)
    )

(use-package elpy
  :ensure t
  :defer t
  :init
  (elpy-enable))

(use-package ein
  :ensure t
  :defer t)

(add-hook 'compilation-start-hook 'compilation-started)
(add-hook 'compilation-finish-functions 'hide-compile-buffer-if-successful)

(defcustom auto-hide-compile-buffer-delay 1
  "Time in seconds before auto hiding compile buffer."
  :group 'compilation
  :type 'number
)

(defun hide-compile-buffer-if-successful (buffer string)
  (setq compilation-total-time (time-subtract nil compilation-start-time))
  (setq time-str (concat " (Time: " (format-time-string "%s.%3N" compilation-total-time) "s)"))

  (if
    (with-current-buffer buffer
      (setq warnings (eval compilation-num-warnings-found))
      (setq warnings-str (concat " (Warnings: " (number-to-string warnings) ")"))
      (setq errors (eval compilation-num-errors-found))

      (if (eq errors 0) nil t)
    )

    ;;If Errors then
    (message (concat "Compiled with Errors" warnings-str time-str))

    ;;If Compiled Successfully or with Warnings then
    (progn
      (bury-buffer buffer)
      (run-with-timer auto-hide-compile-buffer-delay nil 'delete-window (get-buffer-window buffer 'visible))
      (message (concat "Compiled Successfully" warnings-str time-str))
    )
  )
)

(make-variable-buffer-local 'compilation-start-time)

(defun compilation-started (proc) 
  (setq compilation-start-time (current-time))
)

(use-package license-templates
    :ensure t)

(use-package lice
  :ensure t)

(use-package polymode
  :ensure t)
(use-package poly-markdown
  :after polymode
  :ensure t)

(use-package quarto-mode
  :mode (("\\.Rmd" . poly-quarto-mode))
)

;; Example configuration for Consult
(use-package consult
     :ensure t
     ;; Replace bindings. Lazily loaded due by `use-package'.
     :bind (;; C-c bindings in `mode-specific-map'
	    ;; C-x bindings in `ctl-x-map'
	    ("C-x b" . consult-buffer)                ;; orig. switch-to-buffer
	    ("C-x 4 b" . consult-buffer-other-window) ;; orig. switch-to-buffer-other-window
	    ("C-x 5 b" . consult-buffer-other-frame)  ;; orig. switch-to-buffer-other-frame
	    ("C-x r b" . consult-bookmark)            ;; orig. bookmark-jump
	    ("C-x p b" . consult-project-buffer)      ;; orig. project-switch-to-buffer

   )

   ;; Enable automatic preview at point in the *Completions* buffer. This is
 ;; relevant when you use the default completion UI.
 :hook (completion-list-mode . consult-preview-at-point-mode)

 ;; The :init configuration is always executed (Not lazy)
 :init

 ;; Optionally configure the register formatting. This improves the register
 ;; preview for `consult-register', `consult-register-load',
 ;; `consult-register-store' and the Emacs built-ins.
 (setq register-preview-delay 0.5
       register-preview-function #'consult-register-format)

 ;; Optionally tweak the register preview window.
 ;; This adds thin lines, sorting and hides the mode line of the window.
 (advice-add #'register-preview :override #'consult-register-window)

 ;; Use Consult to select xref locations with preview
 (setq xref-show-xrefs-function #'consult-xref
       xref-show-definitions-function #'consult-xref)

 :config
 ;; Optionally configure preview. The default value
 ;; is 'any, such that any key triggers the preview.
 ;; (setq consult-preview-key 'any)
 ;; (setq consult-preview-key "M-.")
 ;; (setq consult-preview-key '("S-<down>" "S-<up>"))
 ;; For some commands and buffer sources it is useful to configure the
 ;; :preview-key on a per-command basis using the `consult-customize' macro.
 (consult-customize
  consult-theme :preview-key '(:debounce 0.2 any)
  consult-ripgrep consult-git-grep consult-grep
  consult-bookmark consult-recent-file consult-xref
  consult--source-bookmark consult--source-file-register
  consult--source-recent-file consult--source-project-recent-file
  ;; :preview-key "M-."
  :preview-key '(:debounce 0.4 any))

 ;; Optionally configure the narrowing key.
 (setq consult-narrow-key "<") ;; "C-+"

 ;; Optionally make narrowing help available in the minibuffer.
 ;; You may want to use `embark-prefix-help-command' or which-key instead.
 ;; (define-key consult-narrow-map (vconcat consult-narrow-key "?") #'consult-narrow-help)

 ;; By default `consult-project-function' uses `project-root' from project.el.
 ;; Optionally configure a different project root function.
 ;;;; 1. project.el (the default)
 ;; (setq consult-project-function #'consult--default-project--function)
 ;;;; 2. vc.el (vc-root-dir)
 ;; (setq consult-project-function (lambda (_) (vc-root-dir)))
 ;;;; 3. locate-dominating-file
 ;; (setq consult-project-function (lambda (_) (locate-dominating-file "." ".git")))
 ;;;; 4. projectile.el (projectile-project-root)
 ;; (autoload 'projectile-project-root "projectile")
 ;; (setq consult-project-function (lambda (_) (projectile-project-root)))
 ;;;; 5. No project support
 ;; (setq consult-project-function nil)
)

;; Enable vertico
(use-package vertico
  :ensure t
  :init
  (vertico-mode)
)

;; Persist history over Emacs restarts. Vertico sorts by history position.
(use-package savehist
  :ensure t
  :init
  (savehist-mode))

;; Enable rich annotations using the Marginalia package
(use-package marginalia
  :ensure t
  ;; Bind `marginalia-cycle' locally in the minibuffer.  To make the binding
  ;; available in the *Completions* buffer, add it to the
  ;; `completion-list-mode-map'.
  :bind (:map minibuffer-local-map
	 ("M-A" . marginalia-cycle))

  :init
  (marginalia-mode))

(use-package hotfuzz
  :ensure t
:init
  (hotfuzz-vertico-mode)
:after vertico
:config
(add-to-list 'completion-styles 'hotfuzz)
)

(use-package fancy-dabbrev
    :ensure t
  :custom
  ;; Ignore several files to complete (in other buffers)
  (dabbrev-ignored-buffer-regexps '("\\.\\(?:pdf\\|jpe?g\\|png\\|py\\|jl\\)\\^'"))
    :config
    ;; Enable fancy-dabbrev previews everywhere:
    (global-fancy-dabbrev-mode)
  ;; Let dabbrev searches ignore case and expansions preserve case:
  (setq dabbrev-case-distinction nil)
  (setq dabbrev-case-fold-search t)
  (setq dabbrev-case-replace nil)
  ;; If you want TAB to indent the line like it usually does when the cursor
  ;; is not next to an expandable word, use 'fancy-dabbrev-expand-or-indent
  ;; instead of `fancy-dabbrev-expand`:
  (global-set-key (kbd "\t") 'fancy-dabbrev-expand-or-indent)
;  (global-set-key (kbd "\t") 'dabbrev-expand)
  (global-set-key (kbd "<backtab>") 'fancy-dabbrev-backward)
  )

(advice-add #'indent-for-tab-command :after #'hippie-expand)

(global-set-key (kbd "TAB") (make-hippie-expand-function
			 '(try-expand-dabbrev-visible
			   try-expand-dabbrev
			   try-expand-dabbrev-all-buffers) t))
    ;(global-set-key "C-;" 'hippie-expand)

(use-package company
      :ensure t
      :config
      (global-company-mode 1)
;; Configuración para hacer que company-mode sea sensible a las mayúsculas
(setq company-dabbrev-downcase t)
(setq company-dabbrev-ignore-case nil)
  )

(use-package org-noter
  :ensure t
  :defer t)

(use-package zotra
:ensure t)

(use-package define-word
:ensure t
:bind("C-c d" . define-word-at-point))

(use-package latex
  :ensure auctex
 :mode
    ("\\.tex\\'" . latex-mode)
  :bind
  (:map LaTeX-mode-map
	("M-<delete>" . TeX-remove-macro)
	("C-c C-r" . reftex-query-replace-document)
	("C-c C-g" . reftex-grep-document))
  )

(use-package reftex
  :ensure t
  :after (latex)
  ;; :bind (:map reftex-toc-map
  ;; 	      ("j" . reftex-toc-next)
  ;; 	      ("k" . reftex-toc-previous))
  :config
  (add-hook 'LaTeX-mode-hook 'turn-on-reftex)
  (add-hook 'TeX-after-compilation-finished-functions #'TeX-revert-document-buffer)
  (setq reftex-plug-into-AUCTeX t))

;; Use pdf-tools to open PDF files
(setq TeX-view-program-selection '((output-pdf "PDF Tools"))
      TeX-source-correlate-start-server t)

;; Update PDF buffers after successful LaTeX runs
(add-hook 'TeX-after-compilation-finished-functions
           #'TeX-revert-document-buffer)

(use-package auctex-latexmk
      :ensure t
      :after (latex)
:config
(auctex-latexmk-setup)
 (setq auctex-latexmk-inherit-TeX-PDF-mode t)
)

(with-eval-after-load "ox-latex"
  (add-to-list 'org-latex-classes
               '("scrartcl" "\\documentclass{scrartcl}"
                 ("\\section{%s}" . "\\section*{%s}")
                 ("\\subsection{%s}" . "\\subsection*{%s}")
                 ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
                 ("\\paragraph{%s}" . "\\paragraph*{%s}")
                 ("\\subparagraph{%s}" . "\\subparagraph*{%s}"))))

(with-eval-after-load "ox-latex"
  (add-to-list 'org-latex-classes
	       '("scrbook" "\\documentclass{scrbook}"
                 ("\\chapter{%s}" . "\\chapter*{%s}")
		 ("\\section{%s}" . "\\section*{%s}")
		 ("\\subsection{%s}" . "\\subsection*{%s}")
		 ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
		 ("\\paragraph{%s}" . "\\paragraph*{%s}")
		 ("\\subparagraph{%s}" . "\\subparagraph*{%s}"))))

(use-package latex-table-wizard
  :ensure t
  :after latex
  :defer t)

(defun my-put-file-name-on-clipboard ()
  "Put the current file name on the clipboard"
  (interactive)
  (let ((filename (if (equal major-mode 'dired-mode)
                      default-directory
                    (buffer-file-name))))
    (when filename
      (with-temp-buffer
        (insert filename)
        (clipboard-kill-region (point-min) (point-max)))
      (message filename))))

(use-package binky
  :hook (after-init-hook . (lambda () (binky-mode) (binky-margin-mode))))

(use-package qrencode
  :ensure t)

(use-package evil
    :ensure t
    :init
    (setq evil-want-integration t) ;; This is optional since it's already set to t by default.
    (setq evil-want-keybinding nil)
    :config
    (evil-mode 1)
    )
    ; Surround 
      (use-package evil-surround
      :ensure t
      :config
      (global-evil-surround-mode 1)
    )
;; (use-package evil-smartparens
;;     :ensure t
;;     :after evil smartparens
;;     :hook (prog-mode . evil-smartparens-mode)
;; )

(use-package evil-matchit
  :after evil
  :ensure t
  :config
  (global-evil-matchit-mode 1)
)

(use-package evil-leader
  :ensure t
  :init
(global-evil-leader-mode)
  :config
  (evil-leader/set-leader "<SPC>")
(evil-leader/set-key "f" 'find-file)
(evil-leader/set-key "F" 'toggle-frame-fullscreen)
(evil-leader/set-key "C" 'my-put-file-name-on-clipboard)
(evil-leader/set-key "m" 'consult-imenu)
(evil-leader/set-key "l" 'consult-line)
(evil-leader/set-key "R" 'recompile)
(evil-leader/set-key "r" 'consult-recent-file)
(evil-leader/set-key "c" 'consult-flymake)
(evil-leader/set-key "g" 'binky-binky)
(evil-leader/set-key "G" 'consult-ripgrep)
(evil-leader/set-key "e" 'eshell)
(evil-leader/set-key "'" 'eshell)
(evil-leader/set-key "v" 'vterm)
(evil-leader/set-key "D" 'dashboard-open)
(evil-leader/set-key "d" 'dired)
(evil-leader/set-key "b" 'consult-buffer)
(evil-leader/set-key "p" 'consult-projectile-switch-project)
(evil-leader/set-key "M" 'notmuch-hello)
(evil-leader/set-key "s" 'consult-flyspell)
(evil-leader/set-key "w" 'writeroom-mode)
(evil-leader/set-key "+" 'global-text-scale-adjust)
(evil-leader/set-key "-" 'global-text-scale-adjust)
(evil-leader/set-key "<SPC>" 'execute-extended-command)
 )

(with-eval-after-load 'evil-maps
   (define-key evil-normal-state-map (kbd "Q") 'kill-this-buffer))

(use-package evil-collection
  :after evil
  :ensure t
  :config
  (evil-collection-init))

(use-package company
:bind (:map company-active-map
       ("C-n" . company-select-next)
       ("C-p" . company-select-previous))
:config
(setq company-idle-delay 0.3)
(global-company-mode t))

(use-package corfu
  :ensure t
  :custom
  (corfu-auto t)          ;; Enable auto completion
  ;; (corfu-separator ?_) ;; Set to orderless separator, if not using space
  :bind
  ;; Another key binding can be used, such as S-SPC.
  ;; (:map corfu-map ("M-SPC" . corfu-insert-separator))
  :init
  (global-corfu-mode))

(use-package dired-rsync
  :ensure t
  :bind (:map dired-mode-map
	      ("C-c C-r" . dired-rsync))
)

(global-set-key (kbd "<f4>") 'tab-next)

(use-package notmuch
  :ensure-system-package (notmuch . notmuch)
  :ensure t
  :defer t)

(add-hook 'dired-mode-hook 'turn-on-gnus-dired-mode)

(use-package notmuch-indicator
  :ensure t
  :after notmuch
:config
  (setq notmuch-indicator-args
	'((:terms "tag:unread" :label "🧐")
	  ; (:terms "tag:todo and tag:urgente" :label "😱")
	  (:terms "--output threads tag:unread and from:amalia" :label "💕")))
  (setq notmuch-indicator-hide-empty-counters t)
  (notmuch-indicator-mode)
)

(use-package notmuch-bookmarks
   :after notmuch
   :config
   (notmuch-bookmarks-mode)
   (notmuch-bookmarks-annotation-mode))

(setq send-mail-function 'sendmail-send-it
      sendmail-program "/usr/bin/msmtp"
      mail-specify-envelope-from t
      message-sendmail-envelope-from 'header
      mail-envelope-from 'header)

(use-package eww-lnum
  :ensure t
  :bind (:map eww-mode-map
  ("f" . eww-lnum-follow)
  ; ("F" . eww-lnum-universal)
  )
)

(use-package system-packages
  :ensure t)

(use-package orgtbl-aggregate
  :ensure t
  :defer t)

(use-package toc-org
  :ensure t
  :hook org-mode)

(use-package citeproc
  :ensure t
  :defer t)

(setq-default org-odt-preferred-output-format "docx")

(use-package org-table-sticky-header
    :ensure t
    :hook (org-mode-hook . org-table-sticky-header-mode)
)

(global-set-key (kbd "<f7>") 'org-latex-export-to-pdf)

(use-package org-rich-yank
  :ensure t
  :bind (:map org-mode-map
	      (("C-M-y" . org-rich-yank))
	      )
)

(use-package empv
		 :ensure t
		 :defer t
	   :config
     (empv-toggle-video)
     (setq empv-invidious-instance "https://invidious.fdn.fr/api/v1")
   (add-to-list 'empv-mpv-args "--ytdl-format=best")
  (add-to-list 'empv-mpv-args "--save-position-on-quit")
  (bind-key "C-x m" empv-map)
)

(use-package mastodon
      :ensure t
    :init
  (setq mastodon-instance-url "https://fosstodon.org")
  (setq mastodon-active-user "@dmolina.fosstodon")
)

(use-package telega
  :ensure t
  :config
 (define-key global-map (kbd "C-c t") telega-prefix-map)
)

(use-package dokuwiki
    :ensure t
    :after markdown
   :mode ("\\.dwiki\\'" . markdown-mode)
    :custom
    (setq dokuwiki-xml-rpc-url "https://doc.danimolina.net/lib/exe/xmlrpc.php")
    (setq dokuwiki-login-user-name "dmolina"))

(use-package ox-zola
	:ensure t
	:after ox-hugo
:vc (:fetcher github :repo gicrisf/ox-zola)
  )

  (use-package ox-hugo
:ensure t   
:pin melpa  
:after ox)
